//
//  CreateTaskViewController.swift
//  PhotonManager
//
//  Created by Altynbek Orumbayev on 6/29/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import UIKit
import SnapKit
import RealmSwift

/// Possible TaskViewControllerStates
///
/// - edit: Task exists and needs to be modified
/// - add: Task does not exist yet
enum TaskControllerState {
    case edit
    case add
}

/// The class representing the TaskViewController
class TaskViewController: UIViewController {
    
    // MARK: - Variables
    
    var state: TaskControllerState = .add
    private var taskToEdit: Task?
    var sectionTitles: [String] = ["    Task Details", "    Keywords"]
    var sections: [[UITableViewCell]] = []
    var mediaAttachment: UIImage?
    
    // MARK: - Subviews
    
    var taskView: TaskView { return self.view as! TaskView }
    var tableView: UITableView { return taskView.tableView }
    var nameInputCell: InputTableCell { return taskView.nameInputCell }
    var descriptionCell: InputTableCell { return taskView.descriptionCell }
    var keywordsCell: KeywordsTableCell { return taskView.keywordsCell }
    var startTaskButton: UIButton { return taskView.startTaskButton }
    
    // MARK: - UITableViewController
    
    init(task: Task? = nil) {
        self.state = task != nil ? .edit : .add
        self.taskToEdit = task
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        setupNavigationBar()
        adjustViewsForState()
        view.setNeedsUpdateConstraints()
    }
    
    override func loadView() {
        let taskView = TaskView()
        taskView.state = self.state
        self.view = taskView
    }
    
    // MARK: - Setup Functions
    
    func setupNavigationBar() {
        let cancel = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(cancelItemPressed))
        
        extendedLayoutIncludesOpaqueBars = true
        navigationItem.leftBarButtonItem = cancel
        navigationItem.largeTitleDisplayMode = .always
        setRightNavigationBarStatus(enabled: false)
    }
    
    func setupViews() {
        sections = [[nameInputCell, descriptionCell], [keywordsCell]]
      
        nameInputCell.delegate = self
        descriptionCell.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        if (self.state == .add) {
            startTaskButton.addTarget(self, action: #selector(doneItemPressed), for: .touchUpInside)
        }
    }
    
    // MARK: - Actions
    
    func adjustViewsForState() {
        if self.state == .edit {
            guard let task = self.taskToEdit else {return}
            navigationItem.title = "Edit Task"
            nameInputCell.inputText = task.name
            descriptionCell.inputText = task.taskDescription
            keywordsCell.fillWithTags(rawTags: task.tags)
            
        } else {
            navigationItem.title = "Add Task"
        }
    }
    
    @objc func doneItemPressed() {
        guard let taskName = self.nameInputCell.inputText else { return }
        guard let taskDescription = self.descriptionCell.inputText else { return }
        let tagsList = self.keywordsCell.tagsList
        
        // If in Edit state, update task remove Controller from navigation stack
        if self.state == .edit {
            guard let task = taskToEdit else {return}
            Database.sharedInstance.updateTaskStatus(task: task,
                                                     name: taskName,
                                                     description: taskDescription,
                                                     tags: tagsList)
            self.navigationController?.popViewController(animated: true)
            
        } else { // If in Add state, either create task or share the media attachement and then create the task and dismiss controller
            let task = Task.init(value: ["name": taskName, "taskDescription": taskDescription, "isActive": false, "isCompleted": false, "tags": tagsList])
            
            if let image = mediaAttachment {
                let imageToShare = [ image ]
                let activityVC = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
                activityVC.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
                    if !completed { return }
    
                    Database.sharedInstance.addTask(task: task)
                    self.dismiss(animated: true, completion: nil)
                }
                self.present(activityVC, animated: true, completion: nil)
            } else {
                Database.sharedInstance.addTask(task: task)
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    @objc func cancelItemPressed() {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - Extensions

extension TaskViewController: InputTableCellDelegate {
    func inputFieldDidChanged() {
        if let name = nameInputCell.inputText, let detail = descriptionCell.inputText {
            self.setRightNavigationBarStatus(enabled: name.count > 0 && detail.count > 0)
        }
    }
}

extension TaskViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return sections[indexPath.section][indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.section == 1 ? 130 : 50
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let currentSection = sections[section] as [UITableViewCell]? else {return 0}
        return currentSection.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
}
