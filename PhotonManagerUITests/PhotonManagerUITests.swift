//
//  PhotonManagerUITests.swift
//  PhotonManagerUITests
//
//  Created by Altynbek Orumbayev on 6/28/18.
//  Copyright © 2018 Altynbek Orumbayev. All rights reserved.
//

import XCTest

class PhotonManagerUITests: XCTestCase {
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /// Simple UI test that attempts to launch add task screen and add simple task
    func testAddTask() {
        
        let app = XCUIApplication()
        app.buttons["centralIcon"].tap()
        
        let tablesQuery2 = app.tables
        let tablesQuery = tablesQuery2
        tablesQuery/*@START_MENU_TOKEN@*/.textFields["Name"]/*[[".cells.textFields[\"Name\"]",".textFields[\"Name\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery2.children(matching: .cell).element(boundBy: 0).children(matching: .textField).element.typeText("Test")
        tablesQuery/*@START_MENU_TOKEN@*/.textFields["Description"]/*[[".cells.textFields[\"Description\"]",".textFields[\"Description\"]"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.tap()
        tablesQuery2.children(matching: .cell).element(boundBy: 1).children(matching: .textField).element.typeText("Description")
        app.navigationBars["Add Task"].buttons["Done"].tap()
        
    }
    
}
